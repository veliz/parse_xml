<?php
namespace App\Model;

use App\Interfaces\ParseInterface;

class Parse implements ParseInterface
{
	public $path;
	
	function __construct($filepath)
	{
		$this->path = $filepath;
	}

	public function parse(){
		return $this->path;
	}
}
?>