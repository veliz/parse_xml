<?php
namespace App\Abstracts;

use App\Interfaces\ParseInterface;


abstract class ParseWrapper implements ParseInterface {

	protected $Iparse;

	public function __construct(ParseInterface $Iparse){
		$this->Iparse = $Iparse;
	}
}

?>