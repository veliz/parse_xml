<?php
namespace App\Classes;

use App\Abstracts\ParseWrapper;
use App\Interfaces\ParseInterface;

class ParseJSON extends ParseWrapper
{
	function __construct(ParseInterface $model){
		parent::__construct($model);
	}
	
	public function parse(){
		$data = file_get_contents($this->Iparse->parse());
		return $products = json_decode($data, true);
	}
}
?>