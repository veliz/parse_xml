<?php
namespace App\Classes;

use App\Abstracts\ParseWrapper;
use App\Interfaces\ParseInterface;

class ParseXML extends ParseWrapper
{

	function __construct(ParseInterface $model){
		parent::__construct($model);
	}

	public function parse(){
		return simplexml_load_file($this->Iparse->parse());
	}
}
?>