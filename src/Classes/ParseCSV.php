<?php
namespace App\Classes;

use App\Abstracts\ParseWrapper;
use App\Interfaces\ParseInterface;

class ParseCSV extends ParseWrapper
{
	function __construct(ParseInterface $model){
		parent::__construct($model);
	}
	
	public function parse(){
		$registros = array();
		if (($fichero = fopen($this->Iparse->parse(), "r")) !== FALSE) {
		    
		    $nombres_campos = fgetcsv($fichero, 0, ",", "\"", "\"");
		    $num_campos = count($nombres_campos);
		
		 	while (($datos = fgetcsv($fichero, 0, ",", "\"", "\"")) !== FALSE) {
		        
		        for ($i = 0; $i < $num_campos; $i++) {
		            $registro[$nombres_campos[$i]] = $datos[$i];
		        }
		        // Añade el registro leido al array de registros
		        $registros[] = $registro;
		    }
		    fclose($fichero);

		    return compact("registros","nombres_campos");
		}else{
			return "";
		}
	}
}
?>