<?php

use App\Classes\ParseXML;
use App\Classes\ParseCSV;
use App\Classes\ParseJSON;
use App\Model\Parse;
use PHPUnit_Framework_TestCase;

class PruebaTest extends PHPUnit_Framework_TestCase
{
	
	public function testLoadXML()
    {
    	$parse = new ParseXML(new Parse('./assets/file/MOCK_DATA.xml'));

    	if(!empty($data = $parse->parse())){
    		 $this->assertTrue(true);
    	}else{
    		$this->assertTrue(false);
    	}
    }

    public function testLoadCSV()
    {
    	$parse = new ParseCSV(new Parse('./assets/file/MOCK_DATA.csv'));

    	if(!empty($data = $parse->parse())){
    		 $this->assertTrue(true);
    	}else{
    		$this->assertTrue(false);
    	}
    }

    public function testLoadJSON()
    {
    	$parse = new ParseJSON(new Parse('./assets/file/MOCK_DATA.json'));

    	if(!empty($data = $parse->parse())){
    		 $this->assertTrue(true);
    	}else{
    		$this->assertTrue(false);
    	}
    }
}
?>